//Modules
import {
	dev_create_origin_spheres,
	dev_show_origin_block,
	dev_create_bloc_volumes,
	mainloop,
	createGroundPlane,
	refreshGeometry,
} from './geo.js';

// Set up
const scene = new THREE.Scene();
const renderer = new THREE.WebGLRenderer({ canvas, antialias: true });
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFShadowMap;
renderer.setPixelRatio(window.devicePixelRatio); // Pops up resolution

// X axis is red.
// Y axis is green.
// Z axis is blue.

// New Camera Class
class CAMERA {
	constructor(window) {
		this.height = window.innerHeight;
		this.width = window.innerWidth;
		this.aspect = this.width / this.height;
		this.main = this.main_camera();
		this.static = this.static_camera();
		this.dev = this.camera_at_origin();
	}
	main_camera() {
		// Animated Camera
		const camera = new THREE.PerspectiveCamera(80, this.aspect, 1, 1400);
		camera.position.set(200, 10, 100); // start
		camera.lookAt(0, 0, 0);
		return camera;
	}
	static_camera() {
		// Block camera at origin for debugging
		const camera2 = new THREE.PerspectiveCamera(60, this.aspect, 1, 1000);
		camera2.position.set(150, 120, 50); // start
		camera2.lookAt(0, 0, 0);
		return camera2;
	}
	camera_at_origin() {
		// Camera at origin for development
		const camera = new THREE.PerspectiveCamera(70, this.aspect, 1, 400);
		camera.position.set(-3, 14, 3); // start
		camera.lookAt(5, 0, 7);
		return camera;
	}
}

function cameraSwing(time) {
	// math set up for camera swing
	const a = 80;
	const b = 60;
	let camera_location = { x: Math.sin(time), y: Math.sin(time), z: Math.cos(time) };
	camera.position.x = a * camera_location.x;
	camera.position.y = 2.5 * camera_location.y + 22;
	camera.position.z = b * camera_location.z;

	// render
	camera.lookAt(0, 0, 0);
	camera.updateProjectionMatrix();
}

// Add helpers Function
function add_helpers(scene, light) {
	// Adds helper to scene
	// returns helper to use as animation object
	const help = new THREE.DirectionalLightHelper(light, 10);
	const axesHelper = new THREE.AxesHelper(120);
	scene.add(help);
	scene.add(axesHelper);
	return help;
}
function render(scene_obj) {
	// dev_create_origin_spheres(scene_obj);
	// dev_create_bloc_volumes(scene_obj);
	// dev_show_origin_block(scene_obj);
	createGroundPlane(scene_obj);
	mainloop(scene_obj, true); // false dev mode
}

// initizatize scene function
function init(scene_obj) {
	// Only called once
	// Fog effect
	{
		const color = 0xffffff; // white
		const near = 10;
		const far = 180;
		scene_obj.fog = new THREE.Fog(color, near, far);
	}

	// Set background color
	scene_obj.background = new THREE.Color(0xffffff);

	//Lighting
	scene_obj.add(new THREE.AmbientLight(0xffffff, 0.25));
	const directionalLight = new THREE.DirectionalLight(0xffffff, 0.6);
	directionalLight.position.set(100, 150, 120);
	directionalLight.target.position.set(0, 0, 0);

	//Lighting Shadow Camera
	const view_n = 150;
	directionalLight.castShadow = true;
	directionalLight.shadow.bias = 0.0001;
	// directionalLight.shadow;

	directionalLight.shadow.camera.right = view_n;
	directionalLight.shadow.camera.left = -view_n;
	directionalLight.shadow.camera.top = view_n;
	directionalLight.shadow.camera.bottom = -view_n;
	directionalLight.shadow.camera.near = 1;
	directionalLight.shadow.camera.far = 400;
	directionalLight.shadow.mapSize.width = 2048 * 2; // default
	directionalLight.shadow.mapSize.height = 2048 * 2; // default

	directionalLight.shadow.camera.updateMatrixWorld();
	scene_obj.add(directionalLight);
	scene_obj.add(directionalLight.target);

	// add helpers to renderer
	// help = add_helpers(scene_obj, directionalLight);

	// add shapes to render
	render(scene_obj);

	return directionalLight;
}

//set up
let help; // Global Var

// Set Lights and Camera
const light = init(scene);
let camera = new CAMERA(window).static; //main static or dev

// Render Geometry
// Animation Loop
const step = 0.005;
let time = 0;
function animate() {
	// update objects
	// help.update();
	light.target.updateMatrixWorld();
	cameraSwing(time); // TOGGLE
	renderer.render(scene, camera);
	requestAnimationFrame(animate);
	// Set up
	time += step;
	if (time > 2 * Math.PI) {
		time = 0;
	}
}

// EVENT Listeners on button.
button.addEventListener('click', () => {
	if (reload > 0) {
		refreshGeometry(scene);
		render(scene);
		animate();
	}
});
