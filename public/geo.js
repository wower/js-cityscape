// Global Materials
// Import out of core three.hs convex module
import { ConvexBufferGeometry } from 'https://unpkg.com/three@0.124.0/examples/jsm/geometries/ConvexGeometry.js';

const material = new THREE.MeshPhongMaterial({ color: 0xffffff }); // white
const ground_material = new THREE.MeshPhongMaterial({
	color: 0xffffff, // White
});

// CLIENT API FUNCTIONS
async function get_single_block(number) {
	// gets block class from server
	let results = {};
	await fetch('/primitive-bloc-vol/' + `${number}`)
		.then((res) => res.json())
		.then((data) => {
			results = data;
		})
		.catch((error) => console.log('Debug | get_single_block Failed', error));
	return results;
}

async function get_all_bloc_corners() {
	// gets block class from server
	let results = [];
	await fetch('/all-bloc-corners/')
		.then((res) => res.json())
		.then((data) => {
			results = data;
		})
		.catch((error) => console.log('Debug | get_all_bloc_corners Failed', error));
	return results;
}

// Geometry Helper Function
function refreshGeometry(scene_obj) {
	// Callback | Removes previous geometry
	scene_obj.traverse(function (child) {
		if (child.name == 'bloc') {
			scene_obj.remove(child);
		}
	});
	scene_obj.traverse(function (child) {
		if (child.name == 'Global-Ground-Plane') {
			scene_obj.remove(child);
		}
	});
	scene_obj.traverse(function (child) {
		if (child.name == 'Primitive-Block-Volume') {
			scene_obj.remove(child);
		}
	});
}

////////////////////////////////////////////////////////////////////
///////       MAIN EXPORTED RENDER FUNCTIONS               ////////
///////////////////////////////////////////////////////////////////

// X axis is SHORT RED | Y axis is Vertical GREEN | Z axis is LONG BLUE.

// Ground Plane
function createGroundPlane(scene_obj) {
	// Universal Ground plane
	const side = 500;
	const ground_plane = new THREE.PlaneGeometry(side, side, 4, 4);

	ground_plane.translate(0, 0, 0);
	ground_plane.rotateX(-1.57079632679);

	const ground_plane_mesh = new THREE.Mesh(ground_plane, ground_material);
	ground_plane_mesh.name = 'Global-Ground-Plane';
	ground_plane_mesh.receiveShadow = true;

	scene_obj.add(ground_plane_mesh);
}

async function dev_create_origin_spheres(scene_obj) {
	if (reload > 0) {
		const offset_array = await get_all_bloc_corners();
		let group = new THREE.Group();
		for (let i = 0; i < offset_array.length; i++) {
			const geo = new THREE.SphereBufferGeometry(1, 32, 32);
			geo.translate(offset_array[i].x, 0, offset_array[i].z);
			// Mesh
			const mes = new THREE.Mesh(geo, material);
			mes.castShadow = true;
			mes.receiveShadow = true;
			//Add
			group.add(mes);
		}
		scene_obj.add(group);
	}
}

async function dev_show_origin_block(scene_obj) {
	// gets first block  at origin only for development
	// set up
	if (reload > 0) {
		let group = new THREE.Group();
		const geometry = await get_single_block(0);
		for (let i = 0; i < geometry.set_of_buildings.length; i++) {
			let points = geometry.set_of_buildings[i].building_vol;
			points = points.map((element) => new THREE.Vector3(element.x, element.y, element.z));
			let parts = new ConvexBufferGeometry(points);
			let mes = new THREE.Mesh(parts, material);
			mes.castShadow = true;
			mes.receiveShadow = true;
			group.add(mes);
		}
		group.name = 'bloc';
		scene_obj.add(group);
	}
}

async function dev_create_bloc_volumes(scene_obj) {
	// Renders whole block as volume for development
	const stats = await report_field_stats();
	let group = new THREE.Group();
	// Loop to create bloc volumes
	for (let i = 0; i < stats.blocks; i++) {
		//set up
		const geometry = await get_single_block(i);
		const local_g_offset = geometry.local_origin_offset;
		const bloc = geometry.primitive_bloc_vol;

		// Draw
		const height_var = bloc.y;

		// create geometry
		const geo = new THREE.BoxBufferGeometry(bloc.x, height_var, bloc.z);

		// Important Global translate
		geo.translate(local_g_offset.x, 0, local_g_offset.z);

		// Local bloc translate
		geo.translate(
			geometry.primitive_bloc_vol.mid.x,
			height_var / 2,
			geometry.primitive_bloc_vol.mid.z
		);

		// Mesh
		const mes = new THREE.Mesh(geo, material);
		mes.castShadow = true;
		mes.receiveShadow = true;
		//Add
		group.name = 'Primitive-Block-Volume';
		group.add(mes);
	}

	// Final ... add to scene
	scene_obj.add(group);
}

async function mainloop(scene_obj, toggle) {
	// Main function to render geometry
	//set up
	const stats = await report_field_stats();
	let max = 0;
	let geometry_buffer = new THREE.Group();
	if (!toggle) {
		max = Math.ceil(stats.blocks / 10);
	} else {
		max = stats.blocks;
	}

	//block loop
	for (let b = 0; b < max; b++) {
		const geometry = await get_single_block(b);
		let group = new THREE.Group();
		const local_g_offset = geometry.local_origin_offset;
		const set = geometry.set_of_buildings;
		for (let i = 0; i < set.length; i++) {
			let points = geometry.set_of_buildings[i].building_vol;
			points = points.map((element) => new THREE.Vector3(element.x, element.y, element.z));
			let parts = new ConvexBufferGeometry(points);
			let mes = new THREE.Mesh(parts, material);
			mes.castShadow = true;
			mes.receiveShadow = true;
			group.add(mes);
		}
		group.translateX(local_g_offset.x);
		group.translateZ(local_g_offset.z);
		geometry_buffer.add(group);
	}
	geometry_buffer.name = 'bloc';
	scene_obj.add(geometry_buffer);
}

export {
	dev_create_origin_spheres,
	dev_create_bloc_volumes,
	dev_show_origin_block,
	createGroundPlane,
	mainloop,
	refreshGeometry,
};
