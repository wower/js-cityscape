class PARAMS {
	// Initial project parameters from client for blocks to field on server | Get ready for JSON.
	// Meters
	constructor() {
		// Site Set up
		this.mode = 'dev'; // Internal variable
		this.info = 'Project Parameters JSON';
		this.field = { x: 200, z: 200 }; // Entire expected field to map from origin
		this.offsets = { x: this.field.x / 2, y: 0, z: this.field.z / 2 }; // Offset to middle of field
		this.offsetorigin = { x: -this.field.x / 2, y: 0, z: -this.field.z / 2 }; // Blocks build in posititive direction from here.

		// Street Options
		this.streetoptions = {
			average: 8, // Approximate number of divsions per block long side
			street_offset: 2, // width of streets to account for on long bloc side
			avenue_offset: 1.5, // width of avenues to account for on short block side
			block_length: 10, // Approximate Length of block of Z axis
			short_side: 5, // Calculate X dist of block
			alley: 0.3, // half width of alley
		};

		// Building Options
		this.buildingoptions = { large: 0, lot: 0, VARIANCE_A: 0, VARIANCE_B: 0, static: 0 }; // Set up
		this.buildingoptions.VARIANCE_A = { prob: 0.8, scaler: 1.6 }; // height jitter, higher has less jitter
		this.buildingoptions.VARIANCE_B = { prob: 0.1, x: 0.5, z: 0.1 }; // parking space jitter
		this.buildingoptions.lot = { prob: 0.1 }; // probability nothing is built in parking space
		this.buildingoptions.large = { prob: 0.01, scaler: 0.6 }; // probability of one block building
		this.buildingoptions.static = 1; // average height
	}
}
