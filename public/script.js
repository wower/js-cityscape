////////////////////////////////////////////////////////////////////////////////////
////////                 Set up                                  ////////////////
////////////////////////////////////////////////////////////////////////////////////
const info_status = document.getElementById('textbox');
const button = document.getElementById('button');
const info_params = document.getElementById('info');

// IMPORTANT Global Vars
let reload = 0;
let client_parameters;

// API CALLING FUNCTIONS
async function post_project_params() {
	// Sends Project Parameters to server for geometry calculations
	client_parameters = JSON.stringify(new PARAMS());
	// console.log(`DEBUG | Client sent parameters init window_status ${payload}`);
	const res = await fetch('/receive-proj-parameters/', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: client_parameters,
	});
	// Client waits for server status and field stats if successfull
	const body = await res.json();
	update(); // updates browser when triggered
	console.log(`Server response: ${body.status}, Server restarts: ${body.restart}`);
}

async function report_field_stats() {
	const formatter = d3.format('.8f');
	// Gets field stats
	let results;
	if (reload == 0) {
		return ' no status yet ';
	} else if (reload > 0) {
		const url = '/get-field-stats/'; // Relative Path
		await fetch(url)
			.then((res) => res.json())
			.then((data) => {
				results = data;
			})
			.catch((error) => console.log('Debug | Failed to report field stats from server', error));
	}
	for (item in results) {
		results[item] = formatter(results[item]);
	}
	return results;
}

function format_params_text() {
	// This can be expanded to delivery more complex html as a string.
	return new PARAMS();
}

// Event Listeners
button.addEventListener('click', () => {
	reload += 1;
	post_project_params();
});

// User Interface updates on change
async function update() {
	const stats = await report_field_stats();
	const params = format_params_text();
	// Get updated data
	info_status.innerText = `Reloads: ${reload} | Dev Mode: ${params.mode}\nStats: ${JSON.stringify(
		stats
	)}`;
	info_params.innerText = `Building options: ${JSON.stringify(params.buildingoptions)}`;
}
update();
