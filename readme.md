# Cityscape Genorator

## Goal

The goal of this project is to use three.js to create a fictional cityscape and fly around it. The buildings are sized by the addition of 2 probability density functions taken from matheamtica and calculated in prob.js hosted on the node.js server

## Update

There are multiple ways the project is growing. This version was recentered around the global origin and increased the number of buildings on the screen.

<img src="src/images/CityscapePlan.png" width="700">

## Examples of Animation

<img src="src/images/example1.png" width="500">

<img src="src/images/example2.png" width="500">
