//Installed node Modules
const express = require('express');
const dotenv = require('dotenv');
const classes = require('./src/classes.js');
const comms = require('./src/comms.js');
const prob = require('./src/prob.js');
const raw_data = require('./src/samples.json');

// Set up Server
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
dotenv.config({ path: './config/config.env' });
const port = process.env.PORT || 5500;
const env = process.env.NODE_ENV;

// IMPORTANT GLOBAL VAR
// IMPORTANT GLOBAL VAR
// IMPORTANT GLOBAL VAR
var restarts = 0;
var fieldinstance = {};
var project_parameters = {};

// LIST OF APIs
app.get('/primitive-bloc-vol/:num', async function (req, res) {
	const vec = fieldinstance.offset_vector_matrix[req.params.num];
	const payload = await new classes.BLOC(vec, project_parameters);
	res.set('Content-Type', 'application/json');
	res.status(201).send(payload);
});

app.post('/receive-proj-parameters/', function (req, res) {
	// insubstaniate global project parameter file
	project_parameters = req.body;

	// insubstaniate global field instance
	fieldinstance = new classes.FIELD(req.body); // Global Var from Client
	// Extra function worker TOGGLE
	// comms.fieldworker(fieldinstance);

	//Resets modelA+modelB index parameters
	prob.process_model();
	restarts++;
	console.log(
		`DEBUG | new field ${project_parameters.info} | receive-proj-parameters ${
			req.method
		} ${req.header('content-type')}\n`
	);
	res.status(201).json({ status: 'OK!', restart: restarts, data: fieldinstance.stats });
});

// app.get('/all-bloc-corners/', async function (req, res) {
// 	const payload = JSON.stringify(fieldinstance.offset_vector_matrix);
// 	res.set('Content-Type', 'application/json');
// 	res.status(201).send(payload);
// });

app.get('/get-field-stats/', async function (req, res) {
	const payload = fieldinstance.stats;
	res.set('Content-Type', 'application/json');
	res.status(201).send(payload);
});

// HOMEPAGE
app.use('/virtual-city', function (req, res) {
	res.status(201).sendFile(__dirname + '/public/index.html');
});

app.use(express.static(__dirname + '/public')); // Gives access to all public files
app.listen(port, () =>
	console.log(`\nServer Listening on port: ${port}\n env: ${env}\n root: ${__dirname}`)
);
