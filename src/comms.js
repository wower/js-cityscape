//Container for more advanced API functions + behaviours
function fieldworker(field_instance) {
	// console.log(`field worker report:\n ${field_instance.offset_vector_matrix}\n`);

	for (let i = 0; i < field_instance.offset_vector_matrix.length; i++) {
		console.log(`field worker report: X ${i}, ${field_instance.offset_vector_matrix[i]}\n`);
		// console.log(`field worker report: Z ${field_instance.offset_vector_matrix[i].z}\n`);
	}

	// console.log(`field worker report:\n ${Object.entries(field_instance.set_of_indexes[0])}\n`);
}

module.exports = {
	fieldworker: fieldworker,
};
