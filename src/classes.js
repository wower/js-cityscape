// Set up Modules
const THREE = require('three');
const d3 = require('d3');
const prob = require('./prob.js');

//Classes
class FIELD {
	// SITE MAP
	// Starts to map corner coordintes and centeroids with heigh index to the field
	constructor(client_parameters) {
		// Set up
		this.params = client_parameters;
		this.standard_centeroid_offset = this.calculate_block_center(
			this.params.streetoptions.block_length,
			this.params.streetoptions.short_side
		); // vector variable // Standard for every block in the field
		this.spacing = this.site_spacing(); // initial list of Xaxis and Zaxis corners accounting for streets and aves
		this.offset_vector_matrix = this.find_corners(this.spacing); // Offset global origin to block local origin corner
		this.scales = prob.build_scales(); // Can be used for more advanced processing of index data
		this.stats = prob.build_stats(this.offset_vector_matrix.length); // Min Max Number etc of stats for analysis | sent back to the client.
	}
	// Methods
	calculate_block_center(length, short) {
		// Finds the center of a block to use find height
		return { x: short / 2, y: 0, z: length / 2 };
	}
	site_spacing() {
		// accounts fo streets and avenues and returns initial list of corner vertexs on perimeter
		let Z = []; // Z BLUE | Longside
		let X = []; // X RED | Short
		for (
			let i = this.params.offsetorigin.z;
			i < this.params.offsets.z;
			i += this.params.streetoptions.short_side + this.params.streetoptions.street_offset
		) {
			X.push(i);
		}
		for (
			let j = this.params.offsetorigin.x;
			j < this.params.offsets.x;
			j += this.params.streetoptions.block_length + this.params.streetoptions.avenue_offset
		) {
			Z.push(j);
		}
		return { xaxis: X, zaxis: Z };
	}
	find_corners(spacings) {
		// Creates array of all bloc origins in 3dvector from intial spacings along perimeter
		let output_Array = []; // Corner points
		for (let z = 0; z < spacings.zaxis.length; z++) {
			for (let x = 0; x < spacings.xaxis.length; x++) {
				output_Array.push(new THREE.Vector3(spacings.xaxis[x], 0, spacings.zaxis[z]));
			}
		}
		return output_Array;
	}
}

class BLOC {
	// A single block with origin at corner
	// X axis is RED | Y axis is GREEN | Z axis is BLUE.
	constructor(local_offset_vec, project_parameters) {
		// Set up for buildings
		this.params = project_parameters;
		this.bloc_viz = true;
		this.local_origin_offset = local_offset_vec; // local origin offset from global origin // 2D vector
		this.dist = this.local_origin_offset.length(); // distance to center of field in 2d vector x,z
		this.index = this.ping_probability_func(); // returns different ways of defining height index of bloc
		this.restricted = this.restricted_array(); // X axis divisions
		// create
		this.primitive_bloc_vol = this.create_bloc_vol(); // block corners in local space // length & wideth of block
		this.parking_spaces = this.wrangle_arrays(this.restricted, this.parking_space_slices()); // Array of z long axis divisions front and back
		this.set_of_buildings = this.building_factory(this.parking_spaces); // array of Buildings from front/back parking spaces
		// Method to create buliding volume within given parking space taking into account calculated height index // VARIANCE VAR
		// Method to add jitter to each building by vector // check to make sure not out of parking space // VARIANCE VAR
	}
	// Methods
	create_bloc_vol() {
		const l = this.params.streetoptions.block_length; // Z axis
		const w = this.params.streetoptions.short_side; // X axis
		const mid_offset_vec = { x: w / 2, z: l / 2 };
		return {
			mid: mid_offset_vec,
			x: w,
			y: height_equation(this.index, l * w, this.dist, this.params.buildingoptions),
			z: l,
		};
	}
	ping_probability_func() {
		return prob.get_prob(this.local_origin_offset.x, this.local_origin_offset.z);
	}
	jitter() {
		// Jitter genorator for parking division pairs
		// set up
		let block = [];
		// Average width of nominal parking space
		let offset = this.params.streetoptions.block_length / this.params.streetoptions.average;

		// GENERAL CASE (equal divisions) for Development & debugging
		// for (let i = 0; i < this.primitive_bloc_vol.z; i = i + offset) {
		// 	block.push(i);
		// }
		// block.push(this.params.streetoptions.block_length);

		// ADVANCED
		// Counts up or down at approximately random interval
		const width_jitter = 0.7;
		if (d3.randomBernoulli(0.5)() == 1) {
			for (
				let i = 0;
				i < this.primitive_bloc_vol.z;
				i = i + offset + Math.abs(d3.randomNormal(0, width_jitter)())
			) {
				block.push(i);
			}
			block.push(this.primitive_bloc_vol.z);
		} else {
			for (
				let j = this.primitive_bloc_vol.z;
				j > 0;
				j = j - offset - Math.abs(d3.randomNormal(0, width_jitter)())
			) {
				block.push(j);
			}
			block.push(0);
		}

		// Remove a division according to some probability
		if (block.length > 3) {
			if (d3.randomBernoulli(0.5)() == 1) {
				const ramplesample = d3.randomInt(1, block.length - 1)();
				block.splice(ramplesample, 1);
			}
		}

		// Makes sure arrays starts at 0
		if (block[0] == 10) {
			block.reverse();
		}
		return block;
	}
	parking_space_slices() {
		// Returns front back pair of divsisions as an array
		const rules = this.params.buildingoptions.large.prob;
		let pair;
		// Divide 2 lengths
		if (d3.randomBernoulli(rules)() == 1) {
			pair = [[0, this.primitive_bloc_vol.z], this.jitter()];
		} else {
			pair = [this.jitter(), this.jitter()];
		}
		return pair;
	}
	restricted_array() {
		// returns x axis divisions for local block as an array
		const offset = this.params.streetoptions.short_side / 2 - this.params.streetoptions.alley;
		return [
			0,
			offset,
			this.params.streetoptions.short_side - offset,
			this.params.streetoptions.short_side,
		];
	}
	wrangle_arrays(xs, zs) {
		// X and Z Divisions as imputs
		// Returns vertexies of parking space rectangle
		let back = [];
		let front = [];
		for (let i = 0; i < zs[0].length - 1; i++) {
			front.push([
				[0, zs[0][i]],
				[0, zs[0][i + 1]],
				[xs[1], zs[0][i]],
				[xs[1], zs[0][i + 1]],
			]);
		}
		for (let j = 0; j < zs[1].length - 1; j++) {
			back.push([
				[xs[2], zs[1][j]],
				[xs[2], zs[1][j + 1]],
				[xs[3], zs[1][j]],
				[xs[3], zs[1][j + 1]],
			]);
		}
		return { front: front, back: back };
	}
	building_factory(coords) {
		// Creates all buildings objects in a bloc returned in a flat array
		// Input is parking space slices.
		const front = coords.front;
		const back = coords.back;
		let delivery = [];
		for (let i = 0; i < front.length; i++) {
			delivery.push(new BUILDING(this.index, front[i], this.dist, this.params));
		}
		for (let j = 0; j < back.length; j++) {
			delivery.push(new BUILDING(this.index, back[j], this.dist, this.params));
		}
		return delivery;
	}
	// X axis is RED | Y axis is GREEN | Z axis is BLUE.
	// end of class
}

class BUILDING {
	// Creates one building volume primitive in local block space
	// Create three js box geometry now?
	constructor(index_num, coords, dist, project_parameters) {
		this.params = project_parameters;
		this.area = this.calculate_area(coords); // in three js method is available
		this.dist = dist; // Magnitute to global origin
		this.index = index_num;
		this.height = height_equation(this.index, this.area, this.dist, this.params.buildingoptions); // Y Axis
		this.building_viz = true;
		this.side2side_jitter = false;
		this.color = 'White';
		this.building_vol = this.create_volume(coords, this.height); // List of 3d vertexs
		this.geometry = coords; // calculated from local origin
	}
	//Method
	calculate_area(coords) {
		// console.log(`DEBUG |  calculate_area coords${coords}\n`);
		if (coords.length == 4) {
			const a = coords[0];
			const b = coords[1];
			const c = coords[2];
			const d = coords[3];
			let x = c[0] - a[0];
			let z = b[1] - a[1];
			return x * z;
		} else {
			return 'debug | calculate area error for length of coordinate array';
		}
	}
	create_volume(coords, height) {
		let arr = [];
		// Foundation y=0;
		for (let i = 0; i < coords.length; i++) {
			arr.push(new THREE.Vector3(coords[i][0], 0, coords[i][1]));
		}

		// top y=height;
		for (let j = 0; j < coords.length; j++) {
			arr.push(new THREE.Vector3(coords[j][0], height, coords[j][1]));
		}
		return arr;
	}
}

// General Height Equation
function height_equation(index, area, dist, options) {
	// Main calculation for height of one building from index / area and stats
	// Set up
	const min_height = 0.2;
	const jitter =
		Math.abs(d3.randomNormal(0, 1 / options.VARIANCE_A.prob)()) * options.VARIANCE_A.scaler;

	// Main Equation
	let height = 0;
	let seed = options.static + Math.pow(10, index.linear) * options.VARIANCE_A.scaler + jitter;

	// Adjustments for area requirements
	if (area < 2) {
		height = seed * 0.5;
	} else if (area >= 2 && area < 21) {
		height = seed;
	} else if (area >= 21) {
		height = seed * options.large.scaler;
	}

	// REPORTS
	// const formatter = d3.format('.3f');
	// console.log(
	// 	`DEBUG | jitter VAR ${[options.VARIANCE_A.prob, formatter(1 / options.VARIANCE_A.prob)]}`
	// );
	// console.log(`DEBUG |              jitter: ${formatter(jitter)}`);
	// console.log(`DEBUG | jitter area ${area}`);
	// console.log(`DEBUG | jitter        index: ${index.index}`);
	// console.log(`DEBUG | jitter index linear: ${index.linear}`);
	// console.log(`DEBUG | jitter index linear: ${Math.pow(10, index.linear)}\n`);
	// console.log(`DEBUG | jitter         seed: ${formatter(seed)}`);
	// console.log(`DEBUG | jitter       HEIGHT: ${formatter(height)}\n`);

	// Final Checks
	if (height < min_height) {
		return min_height;
	} else {
		return height;
	}
}

module.exports = {
	FIELD: FIELD,
	BLOC: BLOC,
	BUILDING: BUILDING,
};
