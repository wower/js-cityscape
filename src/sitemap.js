const d3 = require('d3');
const THREE = require('three');
const prob = require('./prob.js');

// return (arr = arr[0].map((_, colIndex) => arr.map((row) => row[colIndex])));

class PARAMS {
	// Initial project parameters for blocks to field on server | Get ready for JSON.
	// Meters
	constructor() {
		// SITE
		this.info = 'Project Parameters JSON';
		this.field = { x: 100, z: 100 }; // Entire expected field to map from origin
		this.offset2middle = { x: this.field.x / 2, y: 0, z: this.field.z / 2 }; // Offset to middle of field
		this.aspectratio = 2; // Of block rectangle
		this.probabilityvariables = {
			mu: [5, 22],
			sigma: [
				[2508, 2391],
				[2391, 2784],
			],
		};
		// Street options
		this.average = 8; // Apprximate number of divsions per block long side
		this.street_offset = 1.9; // width of streets to account for on long bloc side
		this.avenue_offset = 1.1; /// width of avenues to account for on short block side
		this.block_length = 10; // Approximate Length of block | to be udpated?
		this.short_side = this.block_length / this.aspectratio;
		this.alley = 0.2; // half width of alley
		// Building parameters
		this.buildingoptions = { large: 0, lot: 0, VARIANCE_A: 0, VARIANCE_B: 0, static: 0 }; // Set up
		this.buildingoptions.VARIANCE_A = { prob: 0.4, scaler: 1 }; // height jitter, higher has less jitter
		this.buildingoptions.VARIANCE_B = { prob: 0.1, x: 0.5, z: 0.1 }; // parking space jitter
		this.buildingoptions.lot = { prob: 0.01, tolerance: 0.1 }; // probability nothing is built
		this.buildingoptions.large = { prob: 0.01, scaler: 1 }; // probability of one block building
		this.buildingoptions.static = 1; // average height
	}
}

class FIELD {
	// SITE MAP
	// Starts to map corner coordintes and centeroids with heigh index to the field
	constructor() {
		// Set up
		this.params = new PARAMS();
		this.standard_centeroid_offset = this.calculate_block_center(
			this.params.block_length,
			this.params.short_side
		); // vector variable // Standard for every block in the field
		this.spacing = this.site_spacing(); // initial list of corners
		this.offset_vector_matrix = this.find_corners(this.spacing); // Offset global origin to block local origin corner
		this.pairs = this.create_pairs(this.offset_vector_matrix, this.standard_centeroid_offset); // 4Dvector global origin to bloc centeriod with probably variable
		this.stats = this.get_stats(this.pairs, this.spacing); // Min & Max of indexs for rescaling later.
	}
	// Methods
	site_spacing() {
		// accounts fo streets and avenues and returns initial list of corner vertexs on perimeter
		// This is a bit confusing?
		let Z = []; // Z BLUE | Longside
		let X = []; // X RED | Short
		for (
			let i = 0;
			i < this.params.field.z;
			i += this.params.short_side + this.params.street_offset
		) {
			X.push(i);
		}
		for (
			let j = 0;
			j < this.params.field.x;
			j += this.params.block_length + this.params.avenue_offset
		) {
			Z.push(j);
		}
		return { xaxis: X, zaxis: Z };
	}
	find_corners(spacings) {
		// Creates array of all corner points in 3dvector from intial spacings along perimeter
		let output_Array = []; // Corner points
		for (let z = 0; z < spacings.zaxis.length; z++) {
			for (let x = 0; x < spacings.xaxis.length; x++) {
				output_Array.push(new THREE.Vector3(spacings.xaxis[x], 0, spacings.zaxis[z]));
			}
		}
		return output_Array;
	}
	calculate_block_center(length, short) {
		return { x: short / 2, y: 0, z: length / 2 };
	}
	create_pairs(corners_obj, standard_offset) {
		// First Step adds bloc offset to offset from global
		// forth spot W is the value from the PDF
		var mu = this.params.probabilityvariables.mu; // Center
		var sig = this.params.probabilityvariables.sigma;
		let obj = corners_obj; // corner obj
		const standard_vec = new THREE.Vector2(standard_offset.x, standard_offset.z);
		obj = obj
			.map((vec) => new THREE.Vector2(vec.x, vec.z))
			.map((vec) => vec.add(standard_vec))
			.map((vec) => {
				return {
					x: vec.x,
					y: 0,
					z: vec.y,
					w: prob.get_probability({ x: vec.x, y: vec.y }, mu, sig),
				};
			});
		return obj;
	}
	get_stats(pairs, list) {
		// Maps all values
		const values = [];
		pairs.map((f) => values.push(f.w));
		const myScale = d3
			.scaleLinear()
			.domain([d3.min(values), d3.max(values)])
			.range([0, 10]);
		const logScale = d3
			.scaleLog()
			.domain([d3.min(values), d3.max(values)])
			.range([0, 10]);
		return {
			blocks: values.length,
			min: d3.min(values),
			max: d3.max(values),
			mean: d3.mean(values),
			med: d3.median(values),
			totalx: list.xaxis.slice(-1),
			totalz: list.zaxis.slice(-1),
			scaler: { linear: myScale, log: logScale }, //returns function objects
		};
	}
}

class BLOC {
	// A single block with origin at corner
	// X axis is RED | Y axis is GREEN | Z axis is BLUE.
	constructor(local_offset_vec, mid_vec) {
		// Set up for buildings
		this.params = new PARAMS();
		this.bloc_viz = true;
		this.centeriord = new THREE.Vector3(mid_vec.x, mid_vec.y, mid_vec.z); // 3DVector center of bloc in global space
		this.local_origin_offset = local_offset_vec; // local origin offset from global origin // 2D vector
		this.dist = this.centeriord.distanceTo(
			new THREE.Vector3(
				this.params.probabilityvariables.mu[0],
				0,
				this.params.probabilityvariables.mu[1]
			)
		); // distance to center of field in 2d vector x,z
		this.index = mid_vec.w; // Value of PDF at centeriord of bloc;
		this.alley = this.params.alley; // not used? //Half the full distance from center
		this.restricted = this.restricted_array(); // X axis divisions
		// create
		this.primitive_bloc_vol = this.create_bloc_vol(); // block corners in local space // length & wideth of block
		this.parking_spaces = this.wrangle_arrays(this.restricted, this.parking_space_slices()); // Array of z long axis divisions front and back
		this.set_of_buildings = this.building_factory(this.parking_spaces); // array of Buildings from class
		// Method to create buliding volume within given parking space taking into account calculated height index // VARIANCE VAR
		// Method to add jitter to each building by vector // check to make sure not out of parking space // VARIANCE VAR
	}
	// Methods
	create_bloc_vol() {
		const l = this.params.block_length; // Z axis
		const w = this.params.short_side; // X axis
		const mid_offset_vec = { x: this.params.short_side / 2, z: this.params.block_length / 2 };
		return {
			mid: mid_offset_vec,
			index: this.index,
			x: this.params.short_side,
			y: height_equation(this.index, l * w),
			z: this.params.block_length,
		};
	}
	jitter() {
		// Jitter genorator for parking division pairs
		// Many Various Distribution Variables
		let segments = project_parameters.average;

		let offset = this.primitive_bloc_vol.z / segments;
		let block = [];

		//GENERAL CASE
		// for (let i = 0; i < this.primitive_bloc_vol.z; i = i + offset) {
		// 	block.push(i);
		// }
		// block.push(this.primitive_bloc_vol.z);

		// ADVANCED
		// Counts up or down at approximately random interval
		const width_jitter = 0.7;
		if (d3.randomBernoulli(0.5)() == 1) {
			for (
				let i = 0;
				i < this.primitive_bloc_vol.z;
				i = i + offset + Math.abs(d3.randomNormal(0, width_jitter)())
			) {
				block.push(i);
			}
			block.push(this.primitive_bloc_vol.z);
		} else {
			for (
				let j = this.primitive_bloc_vol.z;
				j > 0;
				j = j - offset - Math.abs(d3.randomNormal(0, width_jitter)())
			) {
				block.push(j);
			}
			block.push(0);
		}

		// Remove a division according to some probability
		if (block.length > 3) {
			if (d3.randomBernoulli(0.5)() == 1) {
				const ramplesample = d3.randomInt(1, block.length - 1)();
				block.splice(ramplesample, 1);
			}
		}

		// Makes sure arrays starts at 0
		if (block[0] == 10) {
			block.reverse();
		}
		// console.log(`Debug | jitter segments ${segments}, length: ${block.length}`);
		// console.log(`Debug | jitter block var ${block}`);
		return block;
	}
	parking_space_slices() {
		// Returns front back pair of divsisions as an array
		const rules = project_parameters.buildingoptions.large.prob;
		let pair;
		// Divide 2 lengths
		if (d3.randomBernoulli(rules)() == 1) {
			pair = [[0, this.primitive_bloc_vol.z], this.jitter()];
		} else {
			pair = [this.jitter(), this.jitter()];
		}
		return pair;
	}
	restricted_array() {
		// returns x axis divisions for local block as an array
		const offset = this.params.short_side / 2 - this.params.alley;
		return [0, offset, this.params.short_side - offset, this.params.short_side];
	}
	wrangle_arrays(xs, zs) {
		// X and Z Divisions as imputs
		// Returns vertexies of parking space rectangle
		let back = [];
		let front = [];
		for (let i = 0; i < zs[0].length - 1; i++) {
			front.push([
				[0, zs[0][i]],
				[0, zs[0][i + 1]],
				[xs[1], zs[0][i]],
				[xs[1], zs[0][i + 1]],
			]);
		}
		for (let j = 0; j < zs[1].length - 1; j++) {
			back.push([
				[xs[2], zs[1][j]],
				[xs[2], zs[1][j + 1]],
				[xs[3], zs[1][j]],
				[xs[3], zs[1][j + 1]],
			]);
		}
		return { front: front, back: back };
	}
	building_factory(coords) {
		// Creates all buildings objects in a bloc returned in a flat array
		// Input is parking space slices.
		const front = coords.front;
		const back = coords.back;
		let delivery = [];
		for (let i = 0; i < front.length; i++) {
			delivery.push(new BUILDING(this.index, front[i]));
		}
		for (let j = 0; j < back.length; j++) {
			delivery.push(new BUILDING(this.index, back[j]));
		}
		return delivery;
	}
	// X axis is RED | Y axis is GREEN | Z axis is BLUE.
	// end of class
}

class BUILDING {
	// Creates one building volume primitive in local block space
	// Create three js box geometry now?
	constructor(index_num, coords) {
		this.params = new PARAMS();
		this.area = this.calculate_area(coords); // in three js method is available
		this.index = index_num;
		this.height = height_equation(this.index, this.area); // Y Axis
		this.building_viz = true;
		this.side2side_jitter = false;
		this.color = 'White';
		this.building_vol = this.create_volume(coords, this.height); // List of 3d vertexs
		this.geometry = coords; // calculated from local origin
	}
	//Method
	calculate_area(coords) {
		// console.log('debug | calculate area');
		if (coords.length == 4) {
			const a = coords[0];
			const b = coords[1];
			const c = coords[2];
			const d = coords[3];
			let x = c[0] - a[0];
			let z = b[1] - a[1];
			return x * z;
		} else {
			return 'debug | calculate area error for length of coordinate array';
		}
	}
	create_volume(coords, height) {
		let arr = [];

		// Foundation y=0;
		for (let i = 0; i < coords.length; i++) {
			arr.push(new THREE.Vector3(coords[i][0], 0, coords[i][1]));
		}

		// top y=height;
		for (let j = 0; j < coords.length; j++) {
			arr.push(new THREE.Vector3(coords[j][0], height, coords[j][1]));
		}
		// console.log(`DEBUG |  create volume ${arr[0]}`);
		return arr;
	}
}

// General Height Equation
function height_equation(index, area) {
	// Main calculation for height of one building from index / area and stats
	// Set up
	const min_height = 0.2;
	let scaling = field.stats.scaler;
	const jitter = Math.abs(
		d3.randomNormal(0, 1 / project_parameters.buildingoptions.VARIANCE_A.prob)()
	);

	// Main Equation
	let height = 0;
	let seed = project_parameters.buildingoptions.static;

	if (area < 3) {
		height =
			seed +
			scaling.linear(index) * 2 +
			jitter * project_parameters.buildingoptions.VARIANCE_A.scaler;
	} else if (area > 40) {
		height = Math.abs(
			seed +
				scaling.linear(index) * 2 +
				jitter * project_parameters.buildingoptions.VARIANCE_A.scaler -
				area * project_parameters.buildingoptions.large.scaler
		);
	} else {
		height =
			seed +
			scaling.linear(index) * 2 +
			jitter * project_parameters.buildingoptions.VARIANCE_A.scaler +
			area * project_parameters.buildingoptions.large.scaler * 0.1;
	}

	// REPORTS
	// console.log(`DEBUG | jitter | ${jitter}`);
	// console.log(`DEBUG | height_equation | ${scaling.linear(index)}`);
	// console.log(`DEBUG | height_equation | ${[height, area]}`);

	// Final Checks
	if (height < min_height) {
		console.log(`DEBUG | min height warning | ${min_height}`);
		return min_height;
	} else {
		return height;
	}
}

////////////////////////////////////////////////////////////////
//          GLOBAL VAR Set up
//             REQUIRED
////////////////////////////////////////////////////////////////

const project_parameters = new PARAMS();
const field = new FIELD();

module.exports = {
	FIELD: FIELD,
	PARAMS: PARAMS,
	BLOC: BLOC,
	BUILDING: BUILDING,
};
