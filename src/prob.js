// Modules
const d3 = require('d3');

////////////////////////////////////////////////////////////////////////////////////////////////
////////                 EXPRESSION JSON PARSER                           //////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

// Relative path to JSON samples file into object
const raw_data = require('../src/samples.json');

// Global Var used in equation and probability functions
var modelA;
var modelB;
var scalers = { linear: {}, log: {}, square: {} };
let results;
function process_model() {
	// Sets the Global probability model A/B on mode js server restart.
	// index 1 is unneeded Mathematica list
	arr = raw_data.slice(1);
	// Returns XY polynomial
	// Only needs to be called once
	// Random selection of random variables
	const seed1 = d3.randomInt(arr.length)();
	const seed2 = d3.randomInt(arr.length)();
	// console.log(`DEBUG | process_model seeds ${[arr[seed1], arr[seed2]]}`);
	// draw
	modelA = equation(arr[seed1]);
	modelB = equation(arr[seed2]);

	//Find Min and Max for new ranges
	let data = [];

	for (var x = -100; x < 101; x++) {
		for (var y = -100; y < 101; y++) {
			data.push(modelA(x, y) + modelB(x, y));
		}
	}

	//Stats
	results = {
		mean: d3.mean(data),
		med: d3.median(data),
		min: d3.min(data),
		max: d3.max(data),
	};

	// Set Global Scalers from samples data points
	scalers.linear = d3.scaleLinear().domain([results.min, results.max]).range([0, 1]);
	scalers.log = d3.scaleLog().domain([results.min, results.max]).range([0, 1]);
	scalers.square = d3.scaleSqrt().domain([results.min, results.max]).range([0, 1]);
}

function equation(seed) {
	// PARSER
	// input is a nested Expression JSON array.
	// a * E^(b * x^2 + x (c + d * y) + y (e + f * y) )
	const raised = seed[2][2];

	const a = seed[1];
	const b = raised[1][1];
	const c = raised[2][2][1];
	const d = raised[2][2][2][1];
	const e = raised[3][1][1];
	const f = raised[3][1][2][1];
	const equ = { a: a, b: b, c: c, d: d, e: e, f: f };
	// Pure function output
	const func = (x, y) =>
		a * Math.pow(Math.E, b * Math.pow(x, 2) + x * (c + d * y) + y * (e + f * y));
	return func;
}

function get_prob(x, y) {
	//This is the main function for the field class to call when creating blocks
	// MAIN CALCULATION PDF ADDITION
	const calculation = modelA(x, y) + modelB(x, y);

	// Structure for output
	obj = {
		index: calculation,
		linear: scalers.linear(calculation),
		log: scalers.log(calculation),
		square: scalers.square(calculation),
	};
	// code to track down numerical error
	// if (calculation < 0.000001) {
	// 	console.log(`DEBUG | get_prob tracking calculation error ${obj.index}`);
	// }

	return obj;
}

function build_scales() {
	// Getting function
	return scalers;
}

function build_stats(blocks) {
	// Getting function
	results.blocks = blocks;
	return results;
}

// Model is set intially here on sever start:)
process_model();

module.exports = {
	process_model: process_model,
	get_prob: get_prob,
	build_scales: build_scales,
	build_stats: build_stats,
};
